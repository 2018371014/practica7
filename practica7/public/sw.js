
//imports
importScripts('js/sw-utils.js')

//Creacion del sw

// Crear constantes para almacenar caché 

const ESTATICO_CACHE = 'static_v2'
const DINAMICO_CACHE = 'dinamico-v1'
const INMUTABLE_CACHE = 'inmutable-v1'

const APP_SHELL = [
    '/',
    'index.html',
    //'styles.css',
    //'img/logo.svg',
    'js/app.js',
    'js/w-utils.js'
]

const APP_SHELL_INMUTABLE = [
    'https://fonts.googleapis.com/css2?family=DM+Sans'
]

// Procesos de instalacion
self.addEventListener('install', (event) =>{
    const cacheStatic = caches.open(ESTATICO_CACHE).then((cache)=>
        cache.addAll(APP_SHELL))
    const cacheInmutable = caches.open(INMUTABLE_CACHE).then((cache)=>
            cache.addAll(APP_SHELL_INMUTABLE))

            event.waitUntil(Promise.all[cacheStatic,cacheInmutable])
})

//Proceso de activacion 
self.addEventListener('activate', (event)=>{
    //Eliminar cache del sw anterior
    const respuesta = caches.keys().then(keys =>{
        keys.forEach(key =>{
            if(key!== ESTATICO_CACHE && key.includes('static')){
                return caches.delete(key)
            }
        });
    });

    event.waitUntil(respuesta)
});
//Estrategias de cache
self.addEventListener('fetch', event=>{

    const respuesta = caches.match(event.request).then(res=>{
        if(res){
            return res
        }else{
            return fetch(event.request).then(newRes =>{
                //Primero ir al archivo sw-utils.js
                return actualizarCacheDinamico(DINAMICO_CACHE, event.request, newRes)
            })
        }
    });
    event.respondWidth(respuesta);
});