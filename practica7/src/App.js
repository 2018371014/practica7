import React from 'react';
import './App.css';
import { Buscador } from './components/buscador/Buscador';
import { Contador } from './components/contador/Contador';
import { Formulario } from './components/formulario/Formulario';
import { Items } from './components/items/Items';
import { Lista } from './components/lista/Lista';
import { Titulo } from './components/titulo/Titulo';

/*const tareasDefault = [
  {text:'Crear los componentes de la app', completed:false},
  {text:'Tarea 2', completed:true},
  {text:'Tarea 3', completed:true},
  {text:'Tarea 4', completed:false}
];*/

function App() {
  // P E R S I S T E N C I A  D E  D A T O S
  const LocalStorageTareas = localStorage.getItem('TAREAS_V1')
  let parsedTareas 
   //Si no ha llenado o ha creado tareas
   if(!LocalStorageTareas){
    //creamos en localstorage como si fuera cache e inicializamos el arreglo en cero
    localStorage.setItem('TAREAS_V1',JSON.stringify([]))
   } else {
    parsedTareas = JSON.parse(LocalStorageTareas)
   }
  // G U A R D A R  T A R E A S 
  const guardarTarea = (newTareas)=>{
    const stringTareas = JSON.stringify(newTareas)
    localStorage.setItem('TAREAS_V1', stringTareas)
    setTareas(newTareas)
  }
  // C O N T A D O R
  //CREAR EL ESTADO
  const [tareas, setTareas ] = React.useState(parsedTareas)
  // Variable que almacene las que estan completadas y las que tienen tareas 
  const completdTareas = tareas.filter(tarea=>!! tarea.completed).length
  // Variable que almacene el total de tareas 
  const totalTareas = tareas.length

  // B U S C A D O R 
  const [buscandoValor, setBuscandoValor] = React.useState('');
  //Crear un array vacio 
  let buscarTareas = tareas
  //Condicional 
  if(!buscandoValor.length >= 1){
    buscarTareas = tareas
  } else {
    buscarTareas = tareas.filter(tarea=>{
      const tareaText = tarea.text.toLowerCase()
      const buscarText = buscandoValor.toLowerCase()

      return tareaText.includes(buscarText)
    })
  }

  //Crear funcion --> Completar tareas
  const completarTarea = (text) => {
    const tareaIndex = tareas.findIndex(tarea => tarea.text == text)
    const nuevasTareas = [...tareas]
    nuevasTareas[tareaIndex].completed = false
    guardarTarea(nuevasTareas)
  }

  //Crear funcion --> Completar tareas
  const desmarcarTarea = (text) => {
    const tareaIndex = tareas.findIndex(tarea => tarea.text == text)
    const nuevasTareas = [...tareas]
    nuevasTareas[tareaIndex].completed = true
    guardarTarea(nuevasTareas)
  }
  //Eliminar tarea 
  const eliminarTarea = (text) =>{
    const nuevasTareas = tareas.filter(tarea => tarea.text !== text)
    guardarTarea(nuevasTareas)
  }
  //Agregar tarea
  const agregarTarea = (text) =>{
    const newTarea = [...tareas]
    newTarea.push({text, completed:true})
    guardarTarea(newTarea)
  }

  return (
    <React.Fragment>
      <Titulo/>
      <Buscador
      buscandoValor={buscandoValor}
      setBuscandoValor={setBuscandoValor}
      />
      <Formulario
      agregarTarea = {agregarTarea}
      />
      <Contador
      total={totalTareas}
      completed={completdTareas}
      />
      <Lista>
        {buscarTareas.map(tareas=>(
          <Items
          text={tareas.text}
          completed={tareas.completed}
          onComplete={()=> completarTarea(tareas.text)}
          onRestore={()=> desmarcarTarea(tareas.text)}
          Delete={()=> eliminarTarea(tareas.text)}
          key={tareas.text}
          />
        ))}
      </Lista>
    </React.Fragment>
  );
}

export default App;