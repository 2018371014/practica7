import React from "react";
import './Buscador.css'
function Buscador({buscandoValor, setBuscandoValor}){
    //Crear el estado 
   // const [buscandoValor, setBuscandoValor] = React.useState('');
    const buscar = (event) =>{
        //console.log(event.target.value);
        setBuscandoValor(event.target.value);
    }
    return(
        <div className="buscador">
            <form className="formulario">
            <input className="buscador-input" placeholder="Buscar tarea... "
            value={buscandoValor}
            onChange={buscar}
            />
            </form>
        </div>
    );
}
export {Buscador}