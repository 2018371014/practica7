import React from "react";
import './Contador.css';

function Contador({total, completed}){
    return(
        <div className="div-contador">
            <h1 className="h1-contador"> Tareas Pendientes {completed} de {total}.</h1>
        </div>
    );
}

export {Contador}