import React from "react";
import './Formulario.css';
function Formulario(props){
    const [newTareaValue, setNewTareaValue] = React.useState('');

    const newtext = (event) =>{
        setNewTareaValue(event.target.value)
    }
    const subm = (event) =>{
        event.preventDefault()
        props.agregarTarea(newTareaValue)
    }
    return(
        <div className="agregar">
            <form className="formulario">
                <input onChange={newtext} className="formulario-input" placeholder="Agregando tarea"></input>
                <button className="formulario-button" onClick={subm}>Crear tarea</button>
            </form>
        </div>
    );
}

export {Formulario}