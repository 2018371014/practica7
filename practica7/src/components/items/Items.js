import React from "react";
import './Items.css';

function Items(props){
    return(
        <div className="item-li">
        <div>
            <span onClick={props.onComplete}> Marcar</span>
        </div>
        <div className={ props.completed ? '':'done'}>
            <span onClick={props.onRestore}>{props.text}</span> 
        </div>
            <span onClick={props.Delete}>Borrar</span>
        </div>
    );
}

export {Items};