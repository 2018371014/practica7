import React from "react";
import './Lista.css';

function Lista(props){
    return(
        <section className="lista-section">
            <ul className="lista-ul">
                {props.children}
            </ul>
        </section>
    );
}
export {Lista};